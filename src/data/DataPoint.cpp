/*
 * DataPoint.cpp
 * Implements a class that represents a data point
 * Created on 11/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "DataPoint.h"
#include <cmath>
#include <climits>

//default constructor
DataPoint::DataPoint()
	: DataPoint(0.0) //call the value constructor
{
	//no code needed
}

//value constructor
DataPoint::DataPoint(double newValue)
	: value(0.0) //zero the field
{
	//and set the value field
	this->setValue(newValue); 
}

//destructor
DataPoint::~DataPoint() {
	value = 0.0; //clear the value field
}

//copy constructor
DataPoint::DataPoint(const DataPoint& dp)
	: value(dp.value) //copy the field
{
	//no code needed
}

//assignment operator
DataPoint& DataPoint::operator=(const DataPoint& src) {
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//getValue method - returns the value of the DataPoint
double DataPoint::getValue() const {
	return this->value; //return the value field
}

//setValue method - sets the value of the DataPoint
void DataPoint::setValue(double newValue) {
	this->value = newValue; //set the value field
}

//negation operator - negates a DataPoint
DataPoint& DataPoint::operator-() {
	this->value = -this->value; //negate the value field
	return *this; //and return the instance
}

//first addition operator - adds two DataPoints together
DataPoint DataPoint::operator+(const DataPoint& other) const {
	return this->value + other.value; //add the values
}

//second addition operator - adds this DataPoint to another
DataPoint& DataPoint::operator+=(const DataPoint& other) {
	this->value += other.value; //update the value field
	return *this; //and return the instance
}

//first subtraction operator - subtracts one DataPoint from another
DataPoint DataPoint::operator-(const DataPoint& other) const {
	return this->value - other.value; //subtract the values
}

//second subtraction operator - subtracts another DataPoint from this one
DataPoint& DataPoint::operator-=(const DataPoint& other) {
	this->value -= other.value; //update the value field
	return *this; //and return the instance
}

//first multiplication operator - multiplies two DataPoints
DataPoint DataPoint::operator*(const DataPoint& other) const {
	return this->value * other.value; //multiply the values
}

//second multiplication operator - multiplies this DataPoint by another
DataPoint& DataPoint::operator*=(const DataPoint& other) {
	this->value *= other.value; //update the value field
	return *this; //and return the instance
}

//first division operator - divides two DataPoints
DataPoint DataPoint::operator/(const DataPoint& other) const {
	return this->value / other.value; //divide the values
}

//second division operator - divides this DataPoint by another
DataPoint& DataPoint::operator/=(const DataPoint& other) {
	this->value /= other.value; //update the value field
	return *this; //and return the instance
}

//equality operator
bool DataPoint::operator==(const DataPoint& other) const {
	//get the comparison components
	double epsilon = std::numeric_limits<double>::epsilon();
	double absDiff = fabs(this->value - other.value);

	//and execute the comparison
	return absDiff < epsilon;
}

//inequality operator
bool DataPoint::operator!=(const DataPoint& other) const {
	return !(*this == other); //use the equality operator
}

//less-than operator
bool DataPoint::operator<(const DataPoint& other) const {
	return this->value < other.value;
}

//greater-than operator
bool DataPoint::operator>(const DataPoint& other) const {
	return this->value > other.value;
}

//less-than-or-equal-to operator
bool DataPoint::operator<=(const DataPoint& other) const {
	return ((*this == other) || (*this < other));
}

//greater-than-or-equal-to operator
bool DataPoint::operator>=(const DataPoint& other) const {
	return ((*this == other) || (*this > other));
}

//stream-out operator
std::ostream& operator<<(std::ostream& os, const DataPoint& dp) {
	os << dp.value; //write the value to the stream
	return os; //and return the stream
}

//stream-in operator
std::istream& operator>>(std::istream& is, DataPoint& dp) {
	is >> dp.value; //read the value from the stream
	return is; //and return the stream
}

//end of implementation
