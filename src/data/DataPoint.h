/*
 * DataPoint.h
 * Declares a class that represents a data point
 * Created on 11/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include <iostream>

//class declaration
class DataPoint {
	//public fields and methods
	public:
		//default constructor
		DataPoint();

		//value constructor
		DataPoint(double newValue);

		//destructor
		virtual ~DataPoint();

		//copy constructor
		DataPoint(const DataPoint& dp);

		//assignment operator
		DataPoint& operator=(const DataPoint& src);

		//getter method
		double getValue() const; //returns the value of the point

		//setter method
		void setValue(double newValue); //sets the point's value

		//overloaded operators
		
		//negates a DataPoint
		DataPoint& operator-();
		
		//adds one DataPoint to another
		DataPoint operator+(const DataPoint& other) const;
		DataPoint& operator+=(const DataPoint& other);

		//subtracts one DataPoint from another
		DataPoint operator-(const DataPoint& other) const;
		DataPoint& operator-=(const DataPoint& other);

		//multiplies one DataPoint by another
		DataPoint operator*(const DataPoint& other) const;
		DataPoint& operator*=(const DataPoint& other);

		//divides one DataPoint by another
		DataPoint operator/(const DataPoint& other) const;
		DataPoint& operator/=(const DataPoint& other);

		//checks to see if two DataPoints are equivalent
		bool operator==(const DataPoint& other) const;
		
		//checks to see if two DataPoints are not equivalent
		bool operator!=(const DataPoint& other) const;

		//checks to see if this DataPoint is less than another
		bool operator<(const DataPoint& other) const;

		//checks to see if this DataPoint is greater than another
		bool operator>(const DataPoint& other) const;

		//checks to see if this DataPoint is less than
		//or equal to another
		bool operator<=(const DataPoint& other) const;

		//checks to see if this DataPoint is greater than
		//or equal to another
		bool operator>=(const DataPoint& other) const;

		//writes the DataPoint to a stream
		friend std::ostream& operator<<(std::ostream& os,
						const DataPoint& dp);

		//reads into the DataPoint from a stream
		friend std::istream& operator>>(std::istream& is,
						DataPoint& dp);

	//protected fields and methods
	protected:
		//field
		double value; //the value of the DataPoint
};

//end of header
