/*
 * DataSet.cpp
 * Implements a class that represents a data set
 * Created on 11/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "DataSet.h"
#include <algorithm>
#include <cmath>

//default constructor
DataSet::DataSet()
	: DataSet(std::vector<DataPoint>()) //call the other constructor
{
	//no code needed
}

//main constructor
DataSet::DataSet(const std::vector<DataPoint>& newData)
	: data() //default the field
{
	//and init the data field
	this->setData(newData); 
}

//destructor
DataSet::~DataSet() {
	this->clear(); //clear the data
}

//copy constructor
DataSet::DataSet(const DataSet& ds)
	: data(ds.data) //copy the field
{
	//no code needed
}

//assignment operator
DataSet& DataSet::operator=(const DataSet& src) {
	this->data = src.data; //assign the data field
	return *this; //and return the instance
}

//getData method - returns the DataSet's data field
const std::vector<DataPoint>& DataSet::getData() const {
	return this->data; //return the data field
}

//getSize method - returns the size of the DataSet
int DataSet::getSize() const {
	return this->data.size();
}

//hasData method - returns whether the DataSet has data
bool DataSet::hasData() const {
	return this->getSize() > 0; //return whether the set has data
}

//getMean method - returns the mean of the DataSet
double DataSet::getMean() const {
	//declare variables
	double ret = 0.0; //the return value
	int len = this->getSize(); //the size of the set

	//get the mean
	if(!this->hasData()) {
		ret = -1;
	} else {
		//loop and calculate the sum of the data
		for(int i = 0; i < len; i++) {
			ret += this->data[i].getValue();
		}

		//and use that sum to calculate the mean
		ret /= static_cast<double>(len);
	}

	//return the mean
	return ret;
}

//getMedian method - returns the median of the data set
double DataSet::getMedian() const {
	//declare variables
	double ret = 0.0;

	//make sure that there is data in the set
	if(!this->hasData()) {
		return -1;
	}

	//copy the data set
	DataSet tmp = *this;

	//sort the copy
	tmp.sortAscending();

	//get the length of the copy
	int len = tmp.getSize();

	//get the median
	if(len % 2 == 0) { //if the length of the set is even
		//get the indexes of the two middle data points
		int midIdx1 = std::floor(len / 2);
		int midIdx2 = std::ceil(len / 2);

		//get the average of the two middle data points
		ret = ((tmp[midIdx1].getValue() +
				tmp[midIdx2].getValue()) / 2.0);
	} else { //if the length of the set is odd
		int midIdx = len / 2;
		ret = tmp[midIdx].getValue();
	}

	//return the median
	return ret;
}

//getMode method - returns the mode of the data set
double DataSet::getMode() const {
	//make sure that there is data in the set
	if(!this->hasData()) {
		return -1;
	}

	//declare variables
	DataSet tmp = *this; //used to find the mode
	DataPoint prev = tmp.data.back();
	DataPoint mode = 0.0;
	int maxCount = 0;
	int curCount = 0;

	//sort the data set
	tmp.sortAscending();

	//loop and get the mode
	for(const auto& elem : tmp.data) {
		if(elem == prev) {
			curCount++;
			if(curCount > maxCount) {
				maxCount = curCount;
				mode = elem;
			}
		} else {
			curCount = 1;
		}
		prev = elem;
	}

	//and return the mode
	return mode.getValue();
}

//getVariance method - returns the variance of the data set
double DataSet::getVariance() const {
	//get the mean of the data set
	double mean = this->getMean();

	//determine whether the set was empty
	if(mean < 0.0) {
		return -1;
	}

	//loop and calculate the unweighted variance
	double ret = 0.0;
	for(int i = 0; i < this->getSize(); i++) {
		ret += std::pow(this->data[i].getValue() - mean, 2);
	}

	//weight the variance
	ret /= this->getSize();

	//and return the weighted variance
	return ret;
}

//getStdDeviation method - returns the standard deviation of the data set
double DataSet::getStdDeviation() const {
	//get the variance of the set
	double variance = this->getVariance();

	//determine whether the set was empty
	if(variance < 0.0) { //if the set was empty
		return -1;
	}

	//return the standard deviation as the square root
	//of the variance
	return std::sqrt(variance);
}

//getMinElement method - returns the minimum element of the data set
DataPoint DataSet::getMinElement() const {
	DataSet tmp = *this; //copy this data set
	tmp.sortAscending(); //sort it in ascending order
	return tmp[0]; //and return the first element
}

//getMaxElement method - returns the maximum element of the data set
DataPoint DataSet::getMaxElement() const {
	DataSet tmp = *this; //copy this data set
	tmp.sortDescending(); //sort it in descending order
	return tmp[0]; //and return the first element
}

//setData method - sets the data for the data set
void DataSet::setData(const std::vector<DataPoint>& newData) {
	this->data = newData; //set the data field
}

//clear method - clears the data set
void DataSet::clear() {
	this->data.clear(); //clear the data field
}

//subscript operator
DataPoint& DataSet::operator[](int idx) {
	return this->data[idx]; //return the data point at the index
}

//first append method - appends a new data point onto the set
void DataSet::append(const DataPoint& point) {
	this->data.push_back(point); //add the point to the data vector
}

//second append method - appends an entire data set onto this set
void DataSet::append(const DataSet& other) {
	for(const auto& elem : other.data) {
		this->append(elem);
	}
}

//sortAscending method - sorts the data set in ascending order
void DataSet::sortAscending() {
	//get a lambda to be used as a sort predicate
	auto cmp = [](const DataPoint& a, const DataPoint& b) {
			return a < b;
	};

	//and sort the data
	std::sort(this->data.begin(), this->data.end(), cmp);
}

//sortDescending method - sorts the data set in descending order
void DataSet::sortDescending() {
	//get a lambda to be used as a sort predicate
	auto cmp = [](const DataPoint& a, const DataPoint& b) {
			return a > b;
	};

	//and sort the data
	std::sort(this->data.begin(), this->data.end(), cmp);
}

//stream-out operator
std::ostream& operator<<(std::ostream& os, const DataSet& ds) {
	//loop and stream out the data
	for(const auto& elem : ds.data) {
		os << elem << " ";
	}

	//and return the stream
	return os;
}

//end of implementation
