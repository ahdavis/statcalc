/*
 * DataSet.h
 * Declares a class that represents a data set
 * Created on 11/1/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <vector>
#include "DataPoint.h"

//class declaration
class DataSet {
	//public fields and methods
	public:
		//default constructor
		DataSet();

		//vector constructor
		DataSet(const std::vector<DataPoint>& newData);

		//destructor
		virtual ~DataSet();

		//copy constructor
		DataSet(const DataSet& ds);

		//assignment operator
		DataSet& operator=(const DataSet& src);

		//getter methods
		
		//returns the underlying data for the set
		const std::vector<DataPoint>& getData() const;
		
		//returns the size of the data set
		int getSize() const;

		//returns whether the set has data
		bool hasData() const;

		//returns the mean of the data set
		//returns a negative value if the set is empty
		double getMean() const;

		//returns the median of the data set
		//returns a negative value if the set is empty
		double getMedian() const;

		//returns the mode of the data set
		//returns a negative value if the set is empty
		double getMode() const;

		//returns the variance of the data set
		//returns a negative value if the set is empty
		double getVariance() const;

		//returns the standard deviation of the data set
		//returns a negative value if the set is empty
		double getStdDeviation() const;

		//returns the minimum element of the data set
		DataPoint getMinElement() const;

		//returns the maximum element of the data set
		DataPoint getMaxElement() const;

		//setter method
		
		//sets the data for the set
		void setData(const std::vector<DataPoint>& newData);

		//other methods
		
		//clears the data set
		void clear();

		//accesses the data point at a specific index of the set
		DataPoint& operator[](int idx);

		//appends a data point to the end of the set
		void append(const DataPoint& point);

		//appends one data set to the end of another
		void append(const DataSet& other);

		//sorts the data set in ascending order
		void sortAscending();

		//sorts the data set in descending order
		void sortDescending();

		//writes the data set to a stream
		friend std::ostream& operator<<(std::ostream& os,
						const DataSet& ds);

	//protected fields and methods
	protected:
		//field
		std::vector<DataPoint> data; //the data for the set
};

//end of header
