/*
 * NormalDistribution.cpp
 * Implements a class that represents a normal distribution
 * Created on 11/5/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "NormalDistribution.h"
#include "../util/factorial.h"
#include <cmath>
#include <climits>

//default constructor
NormalDistribution::NormalDistribution()
	: NormalDistribution(0.0, 1.0) //call the other constructor
{
	//no code needed
}

//first main constructor
NormalDistribution::NormalDistribution(double newMean, double newStdDev)
	: mean(0.0), stdDev(0.0) //init the fields
{
	//set the fields
	this->setMean(newMean);
	this->setStdDeviation(newStdDev);
}

//second main constructor
NormalDistribution::NormalDistribution(const DataSet& data) 
	: NormalDistribution(data.getMean(), data.getStdDeviation())
{
	//no code needed
}

//destructor
NormalDistribution::~NormalDistribution() {
	//no code needed
}

//copy constructor
NormalDistribution::NormalDistribution(const NormalDistribution& nd)
	: mean(nd.mean), stdDev(nd.stdDev)
{
	//no code needed
}

//assignment operator
NormalDistribution& NormalDistribution::operator=(const
					NormalDistribution& src) {
	this->mean = src.mean; //assign the mean
	this->stdDev = src.stdDev; //assign the standard deviation
	return *this; //and return the instance
}

//getMean method - returns the mean of the distribution
double NormalDistribution::getMean() const {
	return this->mean; 
}

//getStdDeviation method - returns the standard deviation
double NormalDistribution::getStdDeviation() const {
	return this->stdDev;
}

//getVariance method - returns the variance of the distribution
double NormalDistribution::getVariance() const {
	return std::pow(this->stdDev, 2); //return the variance
}

//setMean method - sets the mean of the distribution
void NormalDistribution::setMean(double newMean) {
	this->mean = newMean;
}

//setStdDeviation method - sets the standard deviation
void NormalDistribution::setStdDeviation(double newStdDev) {
	if(newStdDev <= 0.0) {
		this->stdDev = std::numeric_limits<double>::epsilon();
	} else {
		this->stdDev = newStdDev;
	}
}

//setVariance method - sets the variance
void NormalDistribution::setVariance(double newVariance) {
	this->setStdDeviation(std::sqrt(newVariance));
}



//first density method - returns the probability density
//from zA to zB
double NormalDistribution::density(double zA, double zB) const {
	//define a constant that represents 
	//the number of divisions to use 
	//in the numeric PDF integration
	const int N_DIV = 1000;
	
	//get a lambda object that defines 
	//the normal distribution function
	auto pdf = [](double x, double mean, double stdDev) {
		//get the adjusted argument value
		double adjValue = std::pow(x - mean, 2);

		//get the doubled variance
		double dblVariance = 2 * std::pow(stdDev, 2);

		//get the exponent of the pdf function
		double exponent = -(adjValue / dblVariance);

		//exponentiate that exponent to get
		//the nonadjusted density value
		double density = std::exp(exponent);

		//get the density multiplier for the function
		double mult = 1.0 / (stdDev * std::sqrt(2 * M_PI));

		//and return the density adjusted
		//by the multiplier
		return mult * density;
	};

	//get a lambda object that maps interval indices
	//to x-values in the integral
	auto intervalMap = [](int i, double a, double dx) {
		return a + (static_cast<double>(i) * dx);
	};

	//calculate delta X for the numeric integration
	double dx = (zB - zA) / static_cast<double>(N_DIV);

	//loop and numerically integrate the PDF function
	double accum = 0.0;
	for(int i = 1; i <= N_DIV; i++) {
		//get the x-values of the current
		//integration integral
		double prevX = intervalMap(i - 1, zA, dx);
		double curX = intervalMap(i, zA, dx);

		//get the value of the PDF for the
		//previous x-value
		double prevPDF = pdf(prevX, this->mean, this->stdDev);

		//get the value of the PDF for the current x-value
		double curPDF = pdf(curX, this->mean, this->stdDev);

		//calculate the area of the current interval
		double curArea = ((prevPDF + curPDF) / 2.0) * dx;

		//and add it to the accumulator
		accum += curArea;
	}

	//return the accumulated area under 
	//the distribution curve
	return accum;
}

//second density method - returns the probability density
//from -z to z
double NormalDistribution::density(double z) const {
	return this->density(-z, z); //call the first density method
}

//densityForLessThan method - returns the density
//for all z-values less than z
double NormalDistribution::densityForLessThan(double z) const {
	//get the density from -z to z
	double density = this->density(z);

	//get the density from -infinity to -z
	double complement = (1.0 - density) / 2.0;

	//and combine the values
	return complement + density;
}

//densityForGreaterThan method - returns the density
//for all z-values greater than z
double NormalDistribution::densityForGreaterThan(double z) const {
	//return the area greater than z
	return 1.0 - this->densityForLessThan(z);
}

//end of implementation
