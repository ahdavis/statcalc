/*
 * NormalDistribution.h
 * Declares a class that represents a normal distribution
 * Created on 11/5/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include "../data/DataSet.h"

//class declaration
class NormalDistribution {
	//public fields and methods
	public:
		//default constructor - defaults to 
		//a mean of 0 and a standard deviation of 1
		NormalDistribution();

		//first main constructor - constructs
		//from a mean and standard deviation
		NormalDistribution(double newMean, double newStdDev);

		//second main constructor - constructs
		//from a known data set
		explicit NormalDistribution(const DataSet& data);

		//destructor
		virtual ~NormalDistribution();

		//copy constructor
		NormalDistribution(const NormalDistribution& nd);

		//assignment operator
		NormalDistribution& operator=(const NormalDistribution& 
						src);

		//getter methods
		
		//returns the mean of the distribution
		double getMean() const;

		//returns the standard deviation of the distribution
		double getStdDeviation() const;

		//returns the variance of the distribution
		double getVariance() const;

		//setter methods
		
		//sets the mean of the distribution
		void setMean(double newMean);

		//sets the standard deviation of the distribution
		void setStdDeviation(double newStdDev);

		//sets the variance of the distribution
		void setVariance(double newVariance);

		//other methods
		
		//returns the probability density
		//from zA to zB
		double density(double zA, double zB) const;
		
		//returns the probability density from
		//-z to z
		double density(double z) const;

		//returns the probability density
		//left of z
		double densityForLessThan(double z) const;

		//returns the probability density
		//right of z
		double densityForGreaterThan(double z) const;

	//protected fields and methods
	protected:
		//fields
		double mean; //the mean of the distribution
		double stdDev; //the standard deviation of the distribution
};

//end of header
