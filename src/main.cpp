/*
 * main.cpp
 * Main code file for statcalc
 * Created on 10/31/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <iostream>
#include <cstdlib>
#include <vector>
#include "dist/NormalDistribution.h"

//main function - main entry point for the program
int main(int argc, char* argv[]) {
	//create a standard normal distribution object
	NormalDistribution dist;

	//print out the density for z < 1.0
	std::cout << "Density for z < 1.0: " 
		<< dist.densityForLessThan(1.0)
		<< std::endl;

	//and return with no errors
	return EXIT_SUCCESS;
}

//end of program
