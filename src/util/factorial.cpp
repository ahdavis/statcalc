/*
 * factorial.cpp
 * Implements a function that calculates the factorial of an integer
 * Created on 11/2/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include the header
#include "factorial.h"

//factorial function - returns the factorial of an integer
unsigned long long factorial(unsigned int n) {
	if(n == 0) {
		return 1;
	} else {
		return n * factorial(n - 1);
	}
}

//end of implementation
